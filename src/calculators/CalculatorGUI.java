package calculators;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;

public class CalculatorGUI extends JFrame implements ActionListener{

	private JPanel contentPane;
	private JTextField txtFirstOperand;
	private JTextField txtSecondOperand;
	private JButton btnAdd, btnSubstract, btnMult, btnDivide;
	private JLabel lblResult;
	private Calculator calculator;

	public CalculatorGUI(){
		init();
	}
	
	/*
	 * Starts and show calculator GUI
	 */
	
	public void init(){
		calculator = new Calculator();
		createGUI();
		setVisible(true);
		setResizable(false);
	}

	/**
	 * Create the frame.
	 */
	public void createGUI() {
		
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 200);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Inserte los operandos y luego presione la operacion deseada");
		lblNewLabel.setBounds(9, 10, 436, 16);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		contentPane.add(lblNewLabel);
		
		txtFirstOperand = new JTextField();
		txtFirstOperand.setBounds(9, 61, 136, 28);
		contentPane.add(txtFirstOperand);
		txtFirstOperand.setColumns(10);
		
		txtSecondOperand = new JTextField();
		txtSecondOperand.setBounds(149, 61, 136, 28);
		contentPane.add(txtSecondOperand);
		txtSecondOperand.setColumns(10);
		
		JLabel label = new JLabel("=");
		label.setBounds(354, 67, 10, 16);
		contentPane.add(label);
		
		btnAdd = new JButton("+");
		btnAdd.setBounds(9, 124, 136, 29);
		
		lblResult = new JLabel("");
		lblResult.setBounds(376, 61, 77, 28);
		lblResult.setBackground(Color.MAGENTA);
		contentPane.add(lblResult);
		contentPane.add(btnAdd);
		
		btnSubstract = new JButton("-");
		btnSubstract.setBounds(149, 124, 136, 29);
		contentPane.add(btnSubstract);
		
		btnMult = new JButton("x");
		btnMult.setBounds(289, 124, 75, 29);
		contentPane.add(btnMult);
		
		btnDivide = new JButton("/");
		btnDivide.setBounds(368, 124, 77, 29);
		contentPane.add(btnDivide);
		
		setListeners(); //Add needed listeners
	}
	
	/**
	 * In order to avoid creating multiple listeners, set the JFrame to listen all button click event
	 */
	
	private void setListeners(){
		btnAdd.addActionListener(this);
		btnSubstract.addActionListener(this);
		btnMult.addActionListener(this);
		btnDivide.addActionListener(this);
	}
	
	/*
	 * Checks if the inserted operands (if any) are correctly formatted
	 * @return true if both operand fields are filled with numbers, false otherwise
	 */
	
	private boolean checkOperands(){
		
		boolean canContinue = false;
		
		String firstOperand = txtFirstOperand.getText();
		String secondOperand = txtSecondOperand.getText();
		
		if(firstOperand.length() > 0 && secondOperand.length() >0){ //Checks if a value was entered
			try{ //Checks if entered value is a number
				Float.parseFloat(firstOperand);
				Float.parseFloat(secondOperand);
				canContinue = true; //If both conditions are met, can continue operation;
			}catch(NumberFormatException exception){
				exception.printStackTrace();
			}
		}
		
		if(! canContinue) //If both conditions aren't met, show error message
			showMessage("Los campos de los operandos deben ser rellenados con numeros");
		
		return canContinue;
		
	}
	
	/*
	 * Shows an error message to the user
	 * @param message Message to be shown in the dialog
	 */
	private void showMessage(String message){
		JOptionPane.showMessageDialog(null, message);
	}

	/*
	 * Implementation of the ActionListener
	 * Evaluates which operation was requested between the inserted operands.
	 */
	
	@Override
	public void actionPerformed(ActionEvent e) {
		JButton pressedButton = (JButton) e.getSource();
		
		if(checkOperands()){
			
			float firstOperand = Float.parseFloat(txtFirstOperand.getText());
			float secondOperand = Float.parseFloat(txtSecondOperand.getText());
			float result = 0;
			boolean success = true;
			//Check the pressed button
			if(pressedButton == btnAdd){
				result = calculator.add(firstOperand, secondOperand);
			}else if(pressedButton == btnSubstract){
				result = calculator.substract(firstOperand, secondOperand);
			}else if(pressedButton == btnMult){
				result = calculator.mult(firstOperand, secondOperand);
			}else if(pressedButton == btnDivide){
				try{
					result = calculator.divide(firstOperand, secondOperand);
				}catch(IllegalArgumentException ex){ //Division by 0. Must show an error.
					success = false;
					showMessage("No es posible dividir entre 0!");
				}
			}
			
			lblResult.setText(""+result);
		}
		
	}
	

}
