package calculators;

public class Calculator {
	
	/**
	 * Returns the sum between two real number.
	 * As addition is a commutative operation, no particular order is expected from the summands
	 * @param firstSummand One of the summands to be added
	 * @param secondSummand The other summand to be added
	 * @return the result of adding the two passed summands
	**/
	public float add(float firstSummand, float secondSummand){
		return firstSummand + secondSummand;
	}
	
	/**
	 * Returns the difference between two real number.
	 * As substraction is not a commutative operation, the first expected parameter is subtrahend and the second is minuend
	 * @param subtrahend Number being substracted
	 * @param minuend Number substracted from the subtrahend
	 * @return difference within subtrahend and minuend 
	**/
	public float substract(float subtrahend, float minuend){
		return subtrahend - minuend;
	}
	
	/**
	 * Returns the product between two real number.
	 * As multiplication is  a commutative operation, no particular order is expected from the multiplicands
	 * @param firstMultiplicand One of the multiplicands to be multiplied
	 * @param secondMultiplicand The other multiplicand to be multiplied
	 * @return product between multiplicands 
	**/
	public float mult(float firstMultiplicand, float secondMultiplicand){
		return firstMultiplicand * secondMultiplicand;
	}
	
	/**
	 * Returns the quotient between two real number.
	 * As division is not a commutative operation, the first expected parameter is dividend and the second is divisor
	 * @param dividend One of the multiplicands to be multiplied
	 * @param divisor The other multiplicand to be multiplied
	 * @return quotient between operators 
	 * @throws IllegalArgumentException if divisor is 0
	**/
	public float divide(float dividend, float divisor){
		if(divisor != 0)
			return dividend / divisor;
		else
			throw new IllegalArgumentException("Argument 'divisor' is 0");
		
	}
	
}
