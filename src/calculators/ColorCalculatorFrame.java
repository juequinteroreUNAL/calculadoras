package calculators;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.JSlider;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import javax.swing.JLabel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import javax.swing.LayoutStyle.ComponentPlacement;

/**
*
* @author Santiago Alvarez
*
*Class in charge of creating the frame for
*the color calculator
*
*/
public class ColorCalculatorFrame extends JFrame {

	private JPanel contentPane;
	private JLabel redLabel;
	private JLabel blueLabel;
	private JSlider blueSlider;
	private JSlider greenSlider;
	private JLabel greenLabel;
	private JSlider redSlider;
	private JPanel colorPanel;


	/**
	 * Create the frame.
	 */
	public ColorCalculatorFrame() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(0, 2, 0, 0));
		
		JPanel slidersPanel = new JPanel();
		contentPane.add(slidersPanel);
		slidersPanel.setLayout(new GridLayout(3, 0, 0, 0));
		
		JPanel redPanel = new JPanel();
		slidersPanel.add(redPanel);
		
		JLabel label = new JLabel("0");
		
		JLabel label_1 = new JLabel("255");
		
		redSlider = new JSlider();
		redSlider.setMaximum(255);
		redSlider.setValue(0);
		
		redLabel = new JLabel("0");
		redLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel lblRed = new JLabel("Rojo");
		lblRed.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_redPanel = new GroupLayout(redPanel);
		gl_redPanel.setHorizontalGroup(
			gl_redPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_redPanel.createSequentialGroup()
					.addGroup(gl_redPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_redPanel.createSequentialGroup()
							.addGap(12)
							.addComponent(label)
							.addGap(68)
							.addComponent(redLabel, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
							.addGap(59)
							.addComponent(label_1))
						.addGroup(gl_redPanel.createSequentialGroup()
							.addGap(5)
							.addComponent(redSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_redPanel.createSequentialGroup()
							.addGap(86)
							.addComponent(lblRed, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_redPanel.setVerticalGroup(
			gl_redPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_redPanel.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_redPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(label)
						.addGroup(gl_redPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(label_1)
							.addComponent(redLabel)))
					.addGap(5)
					.addComponent(redSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblRed, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
		);
		redPanel.setLayout(gl_redPanel);
		
		JPanel greenPanel = new JPanel();
		slidersPanel.add(greenPanel);
		
		JLabel label_6 = new JLabel("0");
		
		greenLabel = new JLabel("0");
		greenLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel label_8 = new JLabel("255");
		
		greenSlider = new JSlider();
		greenSlider.setMaximum(255);
		greenSlider.setValue(0);
		
		JLabel lblGreen = new JLabel("Verde");
		lblGreen.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_greenPanel = new GroupLayout(greenPanel);
		gl_greenPanel.setHorizontalGroup(
			gl_greenPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_greenPanel.createSequentialGroup()
					.addGroup(gl_greenPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_greenPanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(label_6)
							.addGap(70)
							.addComponent(greenLabel, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
							.addGap(60)
							.addComponent(label_8))
						.addGroup(gl_greenPanel.createSequentialGroup()
							.addGap(5)
							.addComponent(greenSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_greenPanel.createSequentialGroup()
							.addGap(82)
							.addComponent(lblGreen, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_greenPanel.setVerticalGroup(
			gl_greenPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_greenPanel.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_greenPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_6)
						.addComponent(label_8)
						.addComponent(greenLabel, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addComponent(greenSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblGreen, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
		);
		greenPanel.setLayout(gl_greenPanel);
		
		JPanel bluePanel = new JPanel();
		slidersPanel.add(bluePanel);
		
		JLabel label_3 = new JLabel("0");
		
		blueLabel = new JLabel("0");
		blueLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JLabel label_5 = new JLabel("255");
		
		blueSlider = new JSlider();
		blueSlider.setValue(0);
		blueSlider.setMaximum(255);
		
		JLabel lblAzul = new JLabel("Azul");
		lblAzul.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_bluePanel = new GroupLayout(bluePanel);
		gl_bluePanel.setHorizontalGroup(
			gl_bluePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_bluePanel.createSequentialGroup()
					.addGroup(gl_bluePanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_bluePanel.createSequentialGroup()
							.addContainerGap()
							.addComponent(label_3)
							.addGap(72)
							.addComponent(blueLabel, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
							.addGap(61)
							.addComponent(label_5))
						.addGroup(gl_bluePanel.createSequentialGroup()
							.addGap(5)
							.addComponent(blueSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_bluePanel.createSequentialGroup()
							.addGap(79)
							.addComponent(lblAzul, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		gl_bluePanel.setVerticalGroup(
			gl_bluePanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_bluePanel.createSequentialGroup()
					.addGap(5)
					.addGroup(gl_bluePanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(label_3)
						.addComponent(label_5)
						.addComponent(blueLabel, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
					.addGap(5)
					.addComponent(blueSlider, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(lblAzul, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		bluePanel.setLayout(gl_bluePanel);
		
		colorPanel = new JPanel();
		contentPane.add(colorPanel);
		
		/*
		 * Add a listener to the redSlider
		 * this will setup the actual value of the slider
		 * into a Label and then
		 * it will call the function SetColor to
		 * update the color
		 */
		redSlider.addChangeListener
		(
				new ChangeListener() 
				{
					
					@Override
					public void stateChanged(ChangeEvent arg0) 
					{
						//Grab the label and set the text to the actual slider value
						redLabel.setText(Integer.toString(redSlider.getValue()));
						//Update the color
						SetColor();
					}
				}
				
		);
		
		/*
		 * Add a listener to the redSlider
		 * this will setup the actual value of the slider
		 * into a Label and then
		 * it will call the function SetColor to
		 * update the color
		 */
		greenSlider.addChangeListener
		(
				new ChangeListener() 
				{
					
					@Override
					public void stateChanged(ChangeEvent arg0) 
					{
						//Grab the label and set the text to the actual slider value
						greenLabel.setText(Integer.toString(greenSlider.getValue()));
						//Update the color
						SetColor();
					}
				}
				
		);
		
		/*
		 * Add a listener to the redSlider
		 * this will setup the actual value of the slider
		 * into a Label and then
		 * it will call the function SetColor to
		 * update the color
		 */
		blueSlider.addChangeListener
		(
				new ChangeListener() 
				{
					
					@Override
					public void stateChanged(ChangeEvent arg0) 
					{
						//Grab the label and set the text to the actual slider value
						blueLabel.setText(Integer.toString(blueSlider.getValue()));
						//Update the color
						SetColor();
					}
				}
				
		);
		
		init();
	}
	
	/*
	 * Starts and show calculator GUI
	 */
	
	public void init(){
		setVisible(true);
		setResizable(false);
		//Invoke this function at start to grab the default color (black)
		SetColor();
	}
	
	/***
	 * This function Updates the color of the panel
	 * each time a Slider is changed
	 */
	void SetColor()
	{
		//Creates a new color based on the sliders values and set it to the background of the panel
		colorPanel.setBackground(new Color(redSlider.getValue(), greenSlider.getValue() ,blueSlider.getValue() ));
	}
}
