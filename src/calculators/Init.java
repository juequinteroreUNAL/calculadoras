package calculators;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * 
 * @author Santiago Alvarez && Juan Esteban Quintero
 *
 *This class setups a main menu to choose one
 *of the calculators to use
 */
public class Init extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Init frame = new Init();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Init() {
		setTitle("Calculadoras Simulaciones");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 150);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Elige la calculadora que deseas usar");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(6, 6, 438, 16);
		contentPane.add(lblNewLabel);
		
		JButton btnNumberCalculator = new JButton("Aritmetica");
		
		//If pressed, we create a new  Calculator Frame
		btnNumberCalculator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new CalculatorGUI();
			}
		});
		
		btnNumberCalculator.setBounds(6, 55, 213, 29);
		contentPane.add(btnNumberCalculator);
		
		JButton btnColorCalculator = new JButton("Colores");
		btnColorCalculator.setBounds(231, 55, 213, 29);
		
		//If pressed, we create a new Color Calculator Frame
		btnColorCalculator.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new ColorCalculatorFrame();
			}
		});
		
		contentPane.add(btnColorCalculator);
	}
}
